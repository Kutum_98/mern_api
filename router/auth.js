const jwt = require("jsonwebtoken");
const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
require("../db/conn");
const User = require("../model/userSchema");



router.get('/', (req, res) => {
    res.send("Hello world from the router server");
});
// using promise
// router.post("/register", (req, res) => {
//     const { name, email, phone, work, password, cpassword } = req.body;
//     if (!name || !email || !phone || !work || !password || !cpassword) {
//         return res.status(422).json({ error: "Plz fill the fields" });
//     }
//     User.findOne({ email: email })
//         .then((userExist) => {
//             if (userExist) {
//                 return res.status(422).json({ error: "email already exist" });
//             }
//             const user = new User({ name, email, phone, work, password, cpassword });
//             user.save().then(() => {
//                 res.status(201).json({ message: "user registered successfully" })
//             }).catch((err) => res.status(500).json({ error: "Failed  to registered" }));

//         }).catch(err => { console.log(err) });
// })
// using async
router.post("/register", async (req, res) => {

    const { name, email, phone, work, password, cpassword } = req.body;
    if (!name || !email || !phone || !work || !password || !cpassword) {
        return res.status(422).json({ error: "Plz fill the fields" });
    }
    try {
        const userExist = await User.findOne({ email: email });
        if (userExist) {
            return res.status(422).json({ error: "email already exist" });
        } else if (password !== cpassword) {
            return res.status(422).json({ error: "pass not matching" });
        } else {

            const user = new User({ name, email, phone, work, password, cpassword });
            //start hashing password

            // end of hashing password
            await user.save();
            res.status(201).json({ message: "user registered successfully" })
        }

    } catch (error) {
        console.log(error);
    }
})

// login route
router.post('/signin', async (req, res) => {
    // console.log(req.body);
    // res.json({ message: "signed in" })
    try {
        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).json({ error: "Please fill the data" });
        }
        const userLogin = await User.findOne({ email: email });
        if (userLogin) {
            const isMatch = await bcrypt.compare(password, userLogin.password);
            const token = await userLogin.generateAuthToken();
            console.log(token);
            res.cookie("jwtoken", token, {
                expires: new Date(Date.now() + 100000),
                httpOnly: true
            });
            if (!isMatch) {
                res.status(400).json({ message: "user error pass" });
            } else {
                res.status(200).json({ message: "user Signin Successfully" });
            }
        } else {
            res.status(400).json({ message: "user error" });
        }


        // console.log(userLogin)
    } catch (error) {
        console.log(error);
    }
})


module.exports = router;