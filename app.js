const dotenv = require("dotenv");
const mongoose = require('mongoose');
const express = require('express');
const app = express();


dotenv.config({ path: './config.env' });
require('./db/conn');
app.use(express.json());
app.use(require("./router/auth"));




// const User = require('./model/userSchema');
const PORT = process.env.PORT;



// middleware\\
const middleware = (req, res, next) => {
    console.log("I am the Middleware");
    next();
}


app.get('/', (req, res) => {
    res.send("Hello world from the server");
})
app.get('/about', middleware, (req, res) => {
    res.send("Hello world from the About server");
})

app.get('/contact', (req, res) => {
    res.cookie("test", "kutum");
    res.send("Hello world from the Contact server");
})

app.get('/signin', (req, res) => {
    res.send("Hello world from the Signin server");
})

app.get('/signup', (req, res) => {
    res.send("Hello world from the Signup server");
})


app.listen(PORT, () => {
    console.log(`server is running at port no ${PORT}`)
})